# Resilience Canonical Object Service

CadMus is a proof of concept to allow to unambiguously specify particular points/regions in different formats and different media to allow citation, linking etc.

## Run the project

### Run the CadMus server

* Get the code  
```git clone https://gitlab.com/scripta/resilience-canonical-object-service.git```  
```$ cd resilience-canonical-object-service/cos```  

* Create a virtualenv and activate it  
```$ vritualenv env && . env/bin/activate```  

* Install requirements (only tornado)  
```$ pip install -r requirements.txt```  

* Run the server  
```$ python serve.py```  

### Simulate the content web server

```$ python3 -m http.server```

Then you can go on http://localhost:8000/example.html

## How it works

The example.html file contains links with a `data-cos-urn` attribute and a small script to enrich those links (cf js/links.js). The script iterate over those links and add a button next to their content.
On pushing the button a request to the cos server is made passing the value of the data attribute which should be a valid cos URN, it returns the links corresponding to the urn. The script then create a popup populated with said links (and embeds).

## URN format

The URNs are very similar to CTS URNs, the regular expression that parses them looks like that:
```urn:cos:(?P<namespace>[^:\.\\]+):(?P<object>[^:\.\\]+):(?P<start>[^:_]+)(_(?P<end>[^:]+))?```

Or in a more readable way:
`urn:cos:<namespace>:<object>:<startURN>(_<endURN>)`
Each URN is a list of uniquely identified and hierarchically ordered parts separated by a dot.

In the example given the namespace is 'ritual' and object is 'mass', so for some of the links the request made to the server are:  
http://localhost:8888/urn/urn:cos:ritual:mass:liturgy-of-the-eucharist  
http://localhost:8888/urn/urn:cos:ritual:mass:liturgy-of-the-eucharist.eucharistic-prayer  

## Data storage

The citation/link data is store in plain json. When receiving a request the server checks if a json file for the queried namespace and object exists, if it does then the current part of the object will be used to generate the different uris. To create those uris we use a template and different keys depending on the 'provider', the provider is simply a host with an api to access different kind of medias, for example youtube is a provider for video content and soundcloud for audio.

A single part is stored like this:
```json
{"name": "a-part-name",
 "urls": [],
 "metadata": {}
 "parts": [],
}
```
`name` has to be unique for this depth of the tree and can not contain a `_`, it is .
`metadata` is a key/value object containing anything, it is sent along with the uris.
`parts` is a list of subparts that follows the same scheme as their parent.
lastly, `urls` is a list of object used to generate the citations/links to different media, the required keys depends on the provider. New providers can be added very easily.
```json
{"content-type": "audio|video|image|link",
 "provider": "soundcloud|youtube|raw",
 ...
}
```

For example:
```json
{"content-type": "video",
 "provider": "youtube",
 "identifier": "9OqCjGmUK8o",
 "start": 10,
 "end": 30
}
```
