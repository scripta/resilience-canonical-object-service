import json
import re

import tornado.ioloop
import tornado.web

# TBD:
# Use https://github.com/music-encoding/schema to point at music scores?
# Make it clear that we Assume a part is a structuring element

# TODO:
# highlight part of the text
# readme

PROVIDERS = {
    'youtube': {
        'video': {
            'parameters': ['identifier', 'start', 'end'],
            'uri_template': 'https://www.youtube.com/watch?v={identifier}&t={start}s',
            'embed_template': 'https://www.youtube.com/embed/{identifier}?start={start}&end={end}'
        }
    },
    'soundcloud': {
        'audio': {
            'parameters': ['identifier', 'start', 'embedid'],
            'uri_template': 'https://soundcloud.com/{identifier}#t={start}',
            'embed_template': 'https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/{embedid}&color=%23ff5500&auto_play=true&hide_related=true&show_comments=false&show_user=true&show_reposts=false&show_teaser=true'
        }
    },
    'raw': {
        'image': {
            'parameters': ['uri'],
            'uri_template': '{uri}',
            'embed_template': '{uri}'
        },
        'link': {
            'parameters': ['uri'],
            'uri_template': '{uri},'
        }
    }
}


class URNHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header("Content-Type", 'application/json')

    def get(self, urn):
        match = re.match(r"urn:cos:(?P<namespace>[^:\.\\]+):(?P<obj>[^:\.\\]+):(?P<start>[^:_]+)(_(?P<end>[^:]+))?", urn)
        if not match:
            raise tornado.web.HTTPError(
                status_code=400,
                reason="Badly formated URN.")

        self.namespace = match.group('namespace')
        self.obj = match.group('obj')
        start = match.group('start')
        end = match.group('end')

        self.read_data()

        starting_parts_names = re.findall(r"[^\.]+", start)
        if end:
            ending_parts_names = re.findall(r"[^\.]+", end)

        if start:
            if not end:
                part = self.get_part(self.data, starting_parts_names)
                body = {
                    "name": part['name'],
                    "metadata": part.get('metadata'),
                    "next": self.get_next_part_urn(self.data, starting_parts_names),
                    # "previous": self.get_previous_part_urn(self.data, starting_parts_names),
                }

                try:
                    body["children"] = [p["name"] for p in part['parts']]
                except KeyError:
                    pass  # no children

                body['urls'] = self.make_urls(part)

            else:
                start_part = self.get_part(self.data, starting_parts_names)
                end_part = self.get_part(self.data, ending_parts_names)

                body = {
                    "name": start_part['name'],
                    "metadata": start_part.get('metadata'),
                    "next": self.get_next_part_urn(self.data, ending_parts_names),
                    # "previous": self.get_previous_part_urn(self.data, starting_parts_names),
                }

                for url in end_part['urls']:
                    # try to find a match in start urls
                    try:
                        match = next(iter([u for u in start_part['urls']
                                           if u['provider'] == url['provider']
                                           and u['content-type'] == url['content-type']]))
                    except (KeyError, StopIteration):
                        pass
                    else:
                        if 'end' in match and 'end' in url:
                            match['end'] = url['end']

                body['urls'] = self.make_urls(start_part)
        else:
            raise tornado.web.HTTPError(
                status_code=404,
                reason="Invalid URN parts.")

        body['urn'] = urn
        self.write(json.dumps(body))

    def read_data(self):
        try:
            namespace = self.namespace
            obj = self.obj
            # Note namespace and obj can not contain dots so that we can't open a file
            # elsewhere on the file system
            with open(f'data/{namespace}/{obj}.json', "r") as fh:
                self.data = json.load(fh)
        except FileNotFoundError:
            raise tornado.web.HTTPError(
                status_code=404,
                reason="Invalid URN Namespace or Object.")

    def make_urls(self, part):
        urls = []
        try:
            for url in part['urls']:
                provider = PROVIDERS[url['provider']][url['content-type']]

                res = {
                    "content": url['content-type'],
                    "uri": provider['uri_template'].format(**url),
                    "provider": url['provider']
                }
                if 'embed_template' in provider:
                    res['embed'] = provider['embed_template'].format(**url)
                    urls.append(res)
        except KeyError:
            raise tornado.web.HTTPError(
                status_code=400,
                reason="Invalid data, missing keys.")
        return urls

    def rec_get_part_names(self, names, tree, index, add):
        # recursively go up the tree
        while abs(index) <= len(names):
            try:
                part = tree[index]['part']['parts'][tree[index]['index']+add]
                part_names = ".".join(names[:index] + [part["name"]])
                return part_names
            except IndexError:
                return self.rec_get_part_names(names, tree, index-1, add)

    def get_next_part_urn(self, data, names):
        part = data  # root
        tree = []
        for i, name in enumerate(names):
            for index, p in enumerate(part['parts']):
                if p['name'].lower() == name.lower():
                    tree.append({'index': index, 'part': part})
                    part = p
                    break

        next_part_names = self.rec_get_part_names(names, tree, -1, +1)
        if next_part_names:
            return "urn:cos:{namespace}:{obj}:{parts}".format(
                namespace=self.namespace,
                obj=self.obj,
                parts=next_part_names)

    def get_previous_part_urn(self, data, names):
        part = data  # root
        tree = []
        for i, name in enumerate(names):
            for index, p in enumerate(part['parts']):
                if p['name'].lower() == name.lower():
                    tree.append({'index': index, 'part': part})
                    part = p
                    break

        prev_part_names = self.rec_get_part_names(names, tree, -1, -1)
        if prev_part_names:
            return "urn:cos:{namespace}:{obj}:{parts}".format(
                namespace=self.namespace,
                obj=self.obj,
                parts=prev_part_names)

    def get_part(self, data, parts):
        try:
            part = data
            for name in parts:
                part = next(iter(p for p in part['parts']
                                 if p['name'].lower() == name.lower()))
        except (KeyError, StopIteration):
            raise tornado.web.HTTPError(
                status_code=404,
                reason="Invalid URN part name.")

        return part


def make_app():
    return tornado.web.Application([
        (r"/urn/([^/]+)", URNHandler),

        # get list of elements in a range urn
        # (r"/urn/range/([^/]+)", URNRangeHandler)
    ])


if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
