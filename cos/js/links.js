var providerIcon = {
    'youtube': {'fab': 'youtube', 'color': 'red'},
    'soundcloud': {'fab':'soundcloud', 'color': 'orange'},
    'raw': {'fas': 'image', 'color': 'grey'}
}

function feedModal(ev, urls) {
    let modal = document.getElementById('COSModal');
    modal.replaceChildren();

    let box = ev.target.getBoundingClientRect();
    modal.style.top = box.top + window.scrollY;
    modal.style.left = box.right;

    urls.forEach(function(link) {
        let div = document.createElement('div');
        let a = document.createElement('a');
        // a.textContent = link.content;
        a.href = link.uri;
        a.target = "_blank";

        // icon
        let i = document.createElement('i')
        i.style.color = providerIcon[link.provider].color;
        if (providerIcon[link.provider].fab) {
            i.classList.add('fab');
            i.classList.add('fa-'+providerIcon[link.provider].fab);
        } else if (providerIcon[link.provider].fas) {
            i.classList.add('fas');
            i.classList.add('fa-'+providerIcon[link.provider].fas);
        }
        a.append(i);

        div.append(a);
        modal.append(div);
        if (link.embed) {
            let embedLink = document.createElement('a');
            // embedLink.textContent = "embed";
            // icon
            let i = document.createElement('i')
            i.classList.add('fas');
            i.classList.add('fa-clone');
            embedLink.append(i);

            if (link.provider == 'youtube' && link.content == 'video') {
                embedLink.addEventListener('click', function(ev) {
                    let iframe = document.createElement('iframe');
                    iframe.width = "560";
                    iframe.height = "315";
                    iframe.src = link.embed;
                    iframe.frameBorder = "0";
                    div.append(iframe);
                    embedLink.style.display = 'none';
                });
            } else if (link.provider == 'soundcloud' && link.content == 'audio') {
                embedLink.addEventListener('click', function(ev) {
                    let iframe = document.createElement('iframe');
                    iframe.height = "166";
                    iframe.src = link.embed;
                    iframe.frameBorder = "0";
                    div.append(iframe);
                    embedLink.style.display = 'none';
                });
            } else if (link.provider == 'raw' && link.content == 'image') {
                embedLink.addEventListener('click', function(ev) {
                    let img = document.createElement('img');
                    img.src = link.embed;
                    img.height = "300";
                    div.append(img);
                    embedLink.style.display = 'none';
                });
            }
            div.append(embedLink);
        }
    });

    modal.style.display = 'block';
}

var highlighted = null;

function highlightText(el, nextUrn) {
    let nextEl = document.querySelector("[data-cos-urn='"+nextUrn+"']");
    var range = document.createRange();
    range.setStart(el, 0);
    if (nextEl !== null) range.setEnd(nextEl, 0);
    else range.setEnd(document.body.lastElementChild, 0);
    div = document.createElement('div');
    div.classList.add('highlight');
    div.appendChild(range.extractContents());
    range.insertNode(div);
    highlighted = div;
};

function showLinksFromURN(ev, urn, el) {
    fetch(COS_SERVER + '/urn/' + urn)
        .then(response => response.json())
        .then(result => {
            if (result.urls) {
                feedModal(ev, result.urls);
                highlightText(el, result.next);
            } else {
                console.error('Error', 'No url found.')
            }
        })
        .catch(error => {
            console.error('Error:', error);
            window.alert('Invalid or incomplete data for this link!');
            ev.target.style.display = 'none';
        });
};

function enrichCOSLinks() {
    let modal = document.createElement('div');
    modal.id = 'COSModal';
    modal.style.position = 'absolute';
    document.addEventListener('click', function(ev) {
        if (!modal.contains(ev.target)) {
            modal.style.display = 'none';
            if (highlighted) {
                highlighted.replaceWith(...highlighted.childNodes);
                hightlighted = null;
            }
        }
    });
    document.body.append(modal);

    var elementsToEnrich = document.querySelectorAll('[data-cos-urn]');
    elementsToEnrich.forEach(function(el, index) {
        // add a button next to the element
        var btn = document.createElement('button');
        btn.textContent = 'cos';
        btn.addEventListener('mouseup', async function(ev) {
            showLinksFromURN(ev, el.dataset.cosUrn, el);
        });
        el.append(btn);
    });
};
